#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp

formulario = """
    <form action= " " method="POST"> 
    <input type="text" name="content">
    <input type="submit" value="Enviar"></form>
"""

class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""

        # Obtenemos el método, el recurso, y el cuerpo de la petición
        method = request.split(' ',2)[0]
        recurso = request.split(' ',2)[1]
        cuerpo = request.split('\n')[-1]

        return (method,recurso,cuerpo)


    def process(self, resourceName):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        method,recurso,cuerpo = resourceName
        if method == 'GET':
            if recurso in self.content.keys():
                httpCode = '200 OK'
                htmlBody ="<html><body><h5>Pagina encontrada</h5>Recurso solicitado:" + recurso + "<br>" \
                          + "Contenido:" + self.content[recurso] + \
                         "<p>Introducir nuevos contenidos</p>" + formulario + "</body></html>"
            else:
                httpCode = '404 Not Found'
                htmlBody = "<html><body><h5>Pagina no encontrada</h5><p> Puedes introducir contenido </p>" + formulario + "</body></html>"
            return(httpCode,htmlBody)
        else: # Method = POST
            self.content[recurso] = cuerpo.split('=')[1] # Nos quedamos con el contenido introducido en el formulario y lo añadimos al diccionario
            httpCode = '200 OK'
            htmlBody = "<html><body><h5>Introducir contenido</h5>Recurso solicitado:" + recurso + "<br>" \
                        + "Contenido:" + self.content[recurso] + formulario + "</body></html>"

        return(httpCode,htmlBody)

if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
